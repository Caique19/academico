package com.araujo.caique.academico.Activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.R;

import java.util.Objects;

public class SignUpPersonalDataActivity extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText password;
    private EditText passwordConfirme;
    private Button btNext;
    private TextView btSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_personal_data);
        btNext = (Button) findViewById(R.id.btNext);

        name = (EditText) findViewById(R.id.nameInput);
        email = (EditText) findViewById(R.id.emailInput);
        password = (EditText) findViewById(R.id.passwordInput);
        passwordConfirme = (EditText) findViewById(R.id.confirmPasswordInput);

        //btNext = (Button) findViewById(R.id.btNext);
        btNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpPersonalDataActivity.this, SignUpContactDataActivity.class);

                if (Objects.equals(name.getText().toString(), "")){
                    Toast.makeText(SignUpPersonalDataActivity.this, "Informe o seu nome", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(email.getText().toString(), "")){
                    Toast.makeText(SignUpPersonalDataActivity.this, "Informe seu e-mail", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(password.getText().toString(), "")){
                    Toast.makeText(SignUpPersonalDataActivity.this, "Informe uma senha", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(passwordConfirme.getText().toString(), "")){
                    Toast.makeText(SignUpPersonalDataActivity.this, "Confirme a senha", Toast.LENGTH_SHORT).show();
                }
                else if (!Objects.equals(password.getText().toString(), passwordConfirme.getText().toString())){
                    Toast.makeText(SignUpPersonalDataActivity.this, "As senhas não correspondem", Toast.LENGTH_SHORT).show();
                }
                else {
                    intent.putExtra("name", name.getText().toString());
                    intent.putExtra("email", email.getText().toString());
                    intent.putExtra("password", password.getText().toString());

                    startActivity(intent);
                }
            }
        });

        btSignIn = (TextView) findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpPersonalDataActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
