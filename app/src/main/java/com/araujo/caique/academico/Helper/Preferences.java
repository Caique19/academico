package com.araujo.caique.academico.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Caique on 21/11/2017.
 */

public class Preferences {

    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String FILE_NAME = "firebase.preferences";
    private int MODE = 0;

    private final String ID_KEY = "userIdLoggedIn";
    private final String NAME_KEY = "userNameLoggedIn";
    private final String EMAIL_KEY = "userEmailLoggedIn";
    private final String CITY_KEY = "userCityLoggedIn";
    private final String PASSWORD_KEY = "userPasswordLoggedIn";
    private final String MYBOOKS_KEY = "userBooksLoggedIn";
    private final String MYLIST_KEY = "userMyListLoggedIn";

    public Preferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(FILE_NAME, MODE);
        editor = preferences.edit();
    }

    public void saveUserPreferences(String userId, String userName, String userEmail, String userCity, String userPassword, String userBooks, String userMyList){
        editor.putString(ID_KEY, userId);
        editor.putString(NAME_KEY, userName);
        editor.putString(EMAIL_KEY, userEmail);
        editor.putString(CITY_KEY, userCity);
        editor.putString(PASSWORD_KEY, userPassword);
        editor.putString(MYBOOKS_KEY, userBooks);
        editor.putString(MYLIST_KEY, userMyList);
        editor.commit();
    }

    public String getID(){
        return preferences.getString(ID_KEY, null);
    }

    public String getName(){
        return  preferences.getString(NAME_KEY, null);
    }

    public String getEmail() {
        return preferences.getString(EMAIL_KEY, null);
    }

    public String getCity() {
        return  preferences.getString(CITY_KEY, null);
    }

    public String getPassword() {
        return preferences.getString(PASSWORD_KEY, null);
    }

    public String getMyBooks() {
        return preferences.getString(MYBOOKS_KEY, null);
    }

    public String getMyList() {
        return preferences.getString(MYLIST_KEY, null);
    }

}
