package com.araujo.caique.academico.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.araujo.caique.academico.Activity.BookDetailsActivity;
import com.araujo.caique.academico.Activity.MainActivity;
import com.araujo.caique.academico.Adapter.MyListAdapter;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.R;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

/**
 * Created by Caique on 08/11/2017.
 */

public class Fragment4 extends Fragment {

    private ArrayList<Book> myList;
    public static ArrayAdapter myListAdapter;

    public static LinearLayout layoutMyList;
    public static TextView textEmptyList;

    public Fragment4() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment4, container, false);

        myList = MainActivity.myList;

        layoutMyList = (LinearLayout) view.findViewById(R.id.layoutMyList);
        textEmptyList = (TextView) view.findViewById(R.id.textEmptyList);

        ListView ltMyList = (ListView) view.findViewById(R.id.ltMyList);
        myListAdapter = new MyListAdapter(getContext(), myList);
        ltMyList.setAdapter(myListAdapter);

        MainActivity.loadMyList();

        ltMyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getContext(), BookDetailsActivity.class);
                intent.putExtra("id", myList.get(position).getId());
                intent.putExtra("name", myList.get(position).getName());
                intent.putExtra("author", myList.get(position).getAuthor());
                intent.putExtra("publishingCompany", myList.get(position).getPublishingCompany());
                intent.putExtra("status", myList.get(position).isAvaliable());
                intent.putExtra("synopsis", myList.get(position).getSynopsis());
                intent.putExtra("genre", myList.get(position).getGenre());
                intent.putExtra("owner", myList.get(position).getOwnerID());
                startActivity(intent);
            }
        });

        return view;
    }

}
