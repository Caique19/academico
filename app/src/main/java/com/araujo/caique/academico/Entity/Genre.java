package com.araujo.caique.academico.Entity;

import android.graphics.drawable.Drawable;

import com.araujo.caique.academico.R;

/**
 * Created by Caique on 14/11/2017.
 */

public class Genre {

    private String name;
    private int icon;

    public Genre(String name, int icon){
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
