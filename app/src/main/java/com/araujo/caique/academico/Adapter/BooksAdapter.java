package com.araujo.caique.academico.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.araujo.caique.academico.Activity.BookDetailsActivity;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.R;

import java.util.ArrayList;

/**
 * Created by Caique on 29/11/2017.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Book> books;

    public BooksAdapter(Context context, ArrayList<Book> myBooks) {
        this.context = context;
        this.books = myBooks;
    }

    @Override
    public BooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(BooksAdapter.ViewHolder holder, final int position) {
        holder.bookName.setText(books.get(position).getName());
        if (books.get(position).isAvaliable()){
            holder.bookStatus.setText("Disponível");
            holder.bookStatus.setTextColor(Color.parseColor("#007700"));
        }
        else {
            holder.bookStatus.setText("Emprestado");
            holder.bookStatus.setTextColor(Color.parseColor("#EE5500"));
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookDetailsActivity.class);
                intent.putExtra("id", books.get(position).getId());
                intent.putExtra("name", books.get(position).getName());
                intent.putExtra("author", books.get(position).getAuthor());
                intent.putExtra("publishingCompany", books.get(position).getPublishingCompany());
                intent.putExtra("status", books.get(position).isAvaliable());
                intent.putExtra("synopsis", books.get(position).getSynopsis());
                intent.putExtra("genre", books.get(position).getGenre());
                intent.putExtra("owner", books.get(position).getOwnerID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout layout;
        public ImageView bookCover;
        public TextView bookName;
        public TextView bookStatus;

        public ViewHolder(View itemView) {
            super(itemView);

            layout = (LinearLayout) itemView.findViewById(R.id.layoutBookItem);
            bookCover = (ImageView) itemView.findViewById(R.id.bookCoverItem);
            bookName = (TextView) itemView.findViewById(R.id.bookNameItem);
            bookStatus = (TextView) itemView.findViewById(R.id.bookStatusItem);

        }
    }
}
