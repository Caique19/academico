package com.araujo.caique.academico.Activity;

import android.content.Intent;
import android.os.Build;
import android.renderscript.Sampler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.araujo.caique.academico.Fragment.Fragment5;
import com.araujo.caique.academico.R;

public class EditProfileActivity extends AppCompatActivity {

    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Mostrar botão de voltar
        getSupportActionBar().setHomeButtonEnabled(true); // Ativar botão de voltar
        setTitle("Editar perfil");

        btCancel = (Button) findViewById(R.id.btCancelEditProfile);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //Ação do botão de voltar
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:break;
        }
        return true;
    }
}
