package com.araujo.caique.academico.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.araujo.caique.academico.Adapter.BooksAdapter;
import com.araujo.caique.academico.Data.FirebaseConfig;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.Fragment.Fragment1;
import com.araujo.caique.academico.Fragment.Fragment4;
import com.araujo.caique.academico.Fragment.Fragment5;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.araujo.caique.academico.Tab.MyAdapter;
import com.araujo.caique.academico.Tab.SlidingTabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    private FirebaseAuth auth;
    private static DatabaseReference firebase;
    public static Preferences preferences;

    public static ArrayList<Book> allBooks;
    public static ArrayList<Book> recentBooks;
    public static ArrayList<Book> myBooks;
    public static ArrayList<Book> myBooksFull;
    public static ArrayList<String> myBooksIds;
    public static ArrayList<Book> myList;
    public static ArrayList<String> myListIds;

    public static Context context;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            finish();
            startActivity(intent);
        }

        preferences = new Preferences(this);

        allBooks = new ArrayList<>();
        recentBooks = new ArrayList<>();
        myBooks = new ArrayList<>();
        myBooksFull = new ArrayList<>();
        myList = new ArrayList<>();

        String userBooksIds = preferences.getMyBooks();
        myBooksIds = new ArrayList<>(Arrays.asList(userBooksIds.split(" ")));

        String userMyListIds = preferences.getMyList();
        myListIds = new ArrayList<>(Arrays.asList(userMyListIds.split(" ")));

        if (Objects.equals(myBooksIds.get(0), "")){
            myBooksIds.clear();
        }

        if (Objects.equals(myListIds.get(0), "")){
            myListIds.clear();
        }

        mViewPager = (ViewPager) findViewById(R.id.vp_tabs);
        mViewPager.setAdapter(new MyAdapter(getSupportFragmentManager(), this));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.white));
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.tv_tab);
        mSlidingTabLayout.setViewPager(mViewPager);

    }

    public  static  void loadAllBooks() {

        allBooks.clear();
        recentBooks.clear();

        firebase = FirebaseConfig.getFirebase().child("book");

        firebase.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    Book book = data.getValue(Book.class);
                    if (!Objects.equals(book.getOwnerID(), preferences.getID())){
                        allBooks.add(book);
                    }
                }

                int cont =0;
                for (int i = allBooks.size() - 1; i >= 0 && cont < 5; i--) {
                    recentBooks.add(allBooks.get(i));
                    cont ++;
                }

                if (recentBooks.size() > 0) {
                    Fragment1.layoutRecentBooks.removeView(Fragment1.textWithoutBooks);
                }

                if (allBooks.size() <= 5) {
                    Fragment1.layoutRecentBooksTitle.removeView(Fragment1.btViewMoreRecentBooks);
                }

                Fragment1.recentBooksAdapter = new BooksAdapter(context, recentBooks);
                Fragment1.recentBooksRecyclerView.setLayoutManager(Fragment1.layoutManager);
                Fragment1.recentBooksRecyclerView.setAdapter(Fragment1.recentBooksAdapter);
                Fragment1.layoutRecentBooks.removeView(Fragment1.loadingRecentBooks);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static void loadMyBooks() {

        myBooks.clear();
        int qtdLivros = 5;

        if (myBooksIds.size() == 0){
            Fragment5.layoutMyBooks.removeView(Fragment5.loadingMyBooks);
        }

        if (myBooksIds.size() > 0){
            Fragment5.layoutMyBooks.removeView(Fragment5.textWithoutBooks);
        }

        if (myBooksIds.size() <= qtdLivros){
            qtdLivros = myBooksIds.size();
            Fragment5.layoutMyBooksTitle.removeView(Fragment5.btViewMore);
        }

        for (int i = myBooksIds.size() - 1; i >= myBooksIds.size() - qtdLivros; i--) {
            firebase = FirebaseConfig.getFirebase().child("book").child(myBooksIds.get(i));
            firebase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Book book = dataSnapshot.getValue(Book.class);
                    myBooks.add(book);
                    Fragment5.myBooksAdapter.notifyDataSetChanged();
                    Fragment5.layoutMyBooks.removeView(Fragment5.loadingMyBooks);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        for (int j = 0; j < myBooksIds.size(); j++) {
            firebase = FirebaseConfig.getFirebase().child("book").child(myBooksIds.get(j));
            firebase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Book book = dataSnapshot.getValue(Book.class);
                    myBooksFull.add(book);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    public static void loadMyList(){
        myList.clear();

        for (int i = 0; i < myListIds.size(); i++) {
            firebase = FirebaseConfig.getFirebase().child("book").child(myListIds.get(i));
            firebase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Book bookInMyList = dataSnapshot.getValue(Book.class);
                    myList.add(bookInMyList);
                    Fragment4.myListAdapter.notifyDataSetChanged();
                    Fragment4.layoutMyList.removeView(Fragment4.textEmptyList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

}
