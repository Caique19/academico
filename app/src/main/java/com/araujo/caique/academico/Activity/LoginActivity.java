package com.araujo.caique.academico.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.Data.FirebaseConfig;
import com.araujo.caique.academico.Entity.User;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private EditText userInput;
    private EditText passwordInput;
    private Button btLogin;
    private TextView btSignUp;
    private FirebaseAuth auth;
    private User user;
    private LinearLayout layoutLogin;
    private ProgressDialog progressDialog;
    private DatabaseReference firebase;
    private User currentUser;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        auth = FirebaseAuth.getInstance();
        preferences = new Preferences(LoginActivity.this);
        user = new User();

        if(auth.getCurrentUser() != null){
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Entrando como " + preferences.getName());
            progressDialog.show();
            user.setEmail(preferences.getEmail());
            user.setPassword(preferences.getPassword());
            validateLogin();
        }

        userInput = (EditText) findViewById(R.id.userInputLogin);
        passwordInput = (EditText) findViewById(R.id.passwordInputLogin);
        layoutLogin = (LinearLayout) findViewById(R.id.linearLayoutLogin);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Entrando...");

        btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                if (!Objects.equals(userInput.getText().toString(), "") && !Objects.equals(passwordInput.getText().toString(), "")){

                    user.setEmail(userInput.getText().toString());
                    user.setPassword(passwordInput.getText().toString());

                    progressDialog.show();

                    validateLogin();

                }
                else {
                    Toast.makeText(LoginActivity.this, "Preencha os campos de e-mail e senha", Toast.LENGTH_SHORT).show();
                }
            }
        });
    
        btSignUp = (TextView) findViewById(R.id.btSignUp);
        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpPersonalDataActivity.class);
                startActivity(intent);
            }
        });
    }

    private void validateLogin(){
        auth = FirebaseConfig.getFirebaseAuth();
        auth.signInWithEmailAndPassword(user.getEmail(), user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    FirebaseUser userFirebase = task.getResult().getUser();
                    firebase = FirebaseConfig.getFirebase().child("user").child(userFirebase.getUid());

                    firebase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            currentUser = dataSnapshot.getValue(User.class);

                            String myBooksIds = "";
                            if(currentUser.getMyBooks() != null) {
                                StringBuilder stringBuilder = new StringBuilder(myBooksIds);
                                for (int i = 0; i < currentUser.getMyBooks().size(); i++) {
                                    stringBuilder.append(currentUser.getMyBooks().get(i) + " ");
                                }
                                myBooksIds = stringBuilder.toString();
                            }

                            String myListIds = "";
                            if(currentUser.getMyList() != null) {
                                StringBuilder stringBuilderMyList = new StringBuilder(myListIds);
                                for (int i = 0; i < currentUser.getMyList().size(); i++) {
                                    stringBuilderMyList.append(currentUser.getMyList().get(i) + " ");
                                }
                                myListIds = stringBuilderMyList.toString();
                            }

                            preferences.saveUserPreferences(currentUser.getId(), currentUser.getName(), currentUser.getEmail(), currentUser.getCity(), currentUser.getPassword(), myBooksIds, myListIds);

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            progressDialog.cancel();
                            finish();
                            startActivity(intent);
                            /*Toast.makeText(LoginActivity.this, "Login efetuado com sucesso!", Toast.LENGTH_SHORT).show();*/
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w("ERRO", "loadUser:onCancelled", databaseError.toException());
                        }
                    });

                }
                else {
                    progressDialog.cancel();
                    Toast.makeText(LoginActivity.this, "E-mail ou senha inválidos!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
