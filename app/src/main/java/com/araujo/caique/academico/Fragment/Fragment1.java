package com.araujo.caique.academico.Fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.araujo.caique.academico.Activity.LoginActivity;
import com.araujo.caique.academico.Activity.MainActivity;
import com.araujo.caique.academico.Activity.ShowBooksActivity;
import com.araujo.caique.academico.Activity.SignUpPersonalDataActivity;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class Fragment1 extends Fragment {

    public static LinearLayout layoutRecentBooks;
    public static LinearLayout layoutRecentBooksTitle;
    public static TextView btViewMoreRecentBooks;
    public static TextView textWithoutBooks;
    public static ProgressBar loadingRecentBooks;

    public static RecyclerView recentBooksRecyclerView;
    public static RecyclerView.LayoutManager layoutManager;
    public static RecyclerView.Adapter recentBooksAdapter;

    private HorizontalScrollView scrollView2;
    private HorizontalScrollView scrollView3;



    public Fragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment1, container, false);

        layoutRecentBooks = (LinearLayout) view.findViewById(R.id.layoutRecentBooks);
        layoutRecentBooksTitle = (LinearLayout) view.findViewById(R.id.layoutRecentBooksTitle);
        btViewMoreRecentBooks = (TextView) view.findViewById(R.id.btViewMoreRecentBooks);
        textWithoutBooks = (TextView) view.findViewById(R.id.textWithoutBooks2);
        loadingRecentBooks = (ProgressBar) view.findViewById(R.id.loadingRecentBooks);

        recentBooksRecyclerView = (RecyclerView) view.findViewById(R.id.recentBooksRecyclerView);
        recentBooksRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        scrollView2 = (HorizontalScrollView) view.findViewById(R.id.scrollView2);
        scrollView3 = (HorizontalScrollView) view.findViewById(R.id.scrollView3);
        scrollView2.setHorizontalScrollBarEnabled(false);
        scrollView3.setHorizontalScrollBarEnabled(false);

        MainActivity.loadAllBooks();

        btViewMoreRecentBooks = (TextView) view.findViewById(R.id.btViewMoreRecentBooks);
        btViewMoreRecentBooks.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View view) {
                Intent intent = new Intent(getContext(), ShowBooksActivity.class);
                intent.putExtra("KEY", "RECENT_BOOKS");
                startActivity(intent);
            }
        });

        return view;

    }
}
