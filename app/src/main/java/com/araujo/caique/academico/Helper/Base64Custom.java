package com.araujo.caique.academico.Helper;

import android.util.Base64;

/**
 * Created by Caique on 21/11/2017.
 */

public class Base64Custom {

    public static String base64Encoding(String text){
        return Base64.encodeToString(text.getBytes(), Base64.DEFAULT).replaceAll("(\\n|\\r)","");
    }

    public static String decodeBase64(String codedText){
        return new String(Base64.decode(codedText, Base64.DEFAULT));
    }

}
