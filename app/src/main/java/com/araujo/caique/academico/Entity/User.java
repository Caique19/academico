package com.araujo.caique.academico.Entity;

import com.araujo.caique.academico.Data.FirebaseConfig;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Caique on 21/11/2017.
 */

public class User {
    private String id;
    private String name;
    private String email;
    private String password;
    private String address;
    private String neighborhood;
    private String city;
    private String phone;
    private String facebook;
    private ArrayList<String> favoriteGenres;
    private ArrayList<String> myBooks;
    private ArrayList<String> myList;
    private ArrayList<String> borrowedForMe;

    public User(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public ArrayList<String> getFavoriteGenres() {
        return favoriteGenres;
    }

    public void setFavoriteGenres(ArrayList<String> favoriteGenres) {
        this.favoriteGenres = favoriteGenres;
    }

    public ArrayList<String> getMyBooks() {
        return myBooks;
    }

    public void setMyBooks(ArrayList<String> myBooks) {
        this.myBooks = myBooks;
    }

    public ArrayList<String> getMyList() {
        return myList;
    }

    public void setMyList(ArrayList<String> myList) {
        this.myList = myList;
    }

    public ArrayList<String> getBorrowedForMe() {
        return borrowedForMe;
    }

    public void setBorrowedForMe(ArrayList<String> borrowedForMe) {
        this.borrowedForMe = borrowedForMe;
    }

    public void register(){
        DatabaseReference firebaseReference = FirebaseConfig.getFirebase();
        firebaseReference.child("user").child(String.valueOf(getId())).setValue(this);
    }

    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> hashMapUser = new HashMap<>();

        hashMapUser.put("id", getId());
        hashMapUser.put("name", getName());
        hashMapUser.put("email", getEmail());
        hashMapUser.put("password", getPassword());
        hashMapUser.put("address", getAddress());
        hashMapUser.put("city", getCity());
        hashMapUser.put("neighborhood", getNeighborhood());
        hashMapUser.put("phone", getPhone());
        hashMapUser.put("facebook", getFacebook());
        hashMapUser.put("favoritesGenres", getFavoriteGenres());
        hashMapUser.put("myBooks", getMyBooks());
        hashMapUser.put("myList", getMyList());
        hashMapUser.put("borrowedForMe", getBorrowedForMe());

        return hashMapUser;
    }
}
