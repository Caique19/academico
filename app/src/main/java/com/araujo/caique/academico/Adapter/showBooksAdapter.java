package com.araujo.caique.academico.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Caique on 14/12/2017.
 */

public class showBooksAdapter extends ArrayAdapter<Book> {

    private final Context context;
    private final ArrayList<Book> books;

    public showBooksAdapter (Context context, ArrayList<Book> books){
        super(context, R.layout.show_books_item, books);
        this.context = context;
        this.books = books;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.show_books_item, parent, false);

        TextView bookName = (TextView) rowView.findViewById(R.id.showBookNameItem);
        TextView bookStatus = (TextView) rowView.findViewById(R.id.showBookStatusItem);


        bookName.setText(books.get(position).getName());
        if (books.get(position).isAvaliable()){
            bookStatus.setText("Disponível");
            bookStatus.setTextColor(Color.parseColor("#007700"));
        }
        else {
            bookStatus.setText("Emprestado");
            bookStatus.setTextColor(Color.parseColor("#EE5500"));
        }

        return rowView;
    }

    private int convertDpToPx(int dp){
        return Math.round(dp*(context.getResources().getDisplayMetrics().xdpi/ DisplayMetrics.DENSITY_DEFAULT));
    }
}
