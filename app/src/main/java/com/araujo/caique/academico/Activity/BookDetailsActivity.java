package com.araujo.caique.academico.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.Data.FirebaseConfig;
import com.araujo.caique.academico.Entity.User;
import com.araujo.caique.academico.Fragment.Fragment4;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class BookDetailsActivity extends AppCompatActivity {

    private TextView bookName;
    private TextView bookAuthor;
    private TextView bookPublishingCompany;
    private TextView bookStatus;
    private TextView bookSynopsis;
    private LinearLayout btLocation;
    private TextView bookGenreName;
    private ImageView bookGenreIcon;
    private Button btAddMyList;
    private LinearLayout layoutOwner;
    private TextView ownerName;
    private TextView ownerAddress;
    private ProgressBar loadingOwner;

    private String id;
    private String name;
    private String author;
    private String publishingCompany;
    private Boolean status;
    private String synopsis;
    private String genre;
    private String ownerID;

    private ArrayList<String> myList;
    private User owner;

    private DatabaseReference firebase;
    private Preferences preferences;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Mostrar botão de voltar
        getSupportActionBar().setHomeButtonEnabled(true); // Ativar botão de voltar

        final Intent intent = getIntent();
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        author = intent.getStringExtra("author");
        publishingCompany = intent.getStringExtra("publishingCompany");
        status = intent.getBooleanExtra("status", true);
        synopsis = intent.getStringExtra("synopsis");
        genre = intent.getStringExtra("genre");
        ownerID = intent.getStringExtra("owner");

        setTitle(name);

        bookName = (TextView) findViewById(R.id.nameBookDetails);
        bookAuthor = (TextView) findViewById(R.id.authorBookDetails);
        bookPublishingCompany = (TextView) findViewById(R.id.publishingCompanyBookDetails);
        bookStatus = (TextView) findViewById(R.id.statusBookDetails);
        bookSynopsis = (TextView) findViewById(R.id.synopsisBookDetails);
        btLocation = (LinearLayout) findViewById(R.id.btLocation);
        bookGenreName = (TextView) findViewById(R.id.genreNameBookDetails);
        bookGenreIcon = (ImageView) findViewById(R.id.genreIconBookDetails);
        btAddMyList = (Button) findViewById(R.id.btAddMyList);
        layoutOwner = (LinearLayout) findViewById(R.id.layoutOwnerBookDetails);
        ownerName = (TextView) findViewById(R.id.ownerNameBookDetails);
        ownerAddress = (TextView) findViewById(R.id.ownerAddressBookDetails);
        loadingOwner = (ProgressBar) findViewById(R.id.loadingOwnerBookDetails);

        bookName.setText(name);
        bookAuthor.setText(author);
        bookPublishingCompany.setText(publishingCompany);
        bookGenreName.setText(genre);
        bookSynopsis.setText(synopsis);
        if (status){
            bookStatus.setText("Disponível");
            bookStatus.setTextColor(Color.parseColor("#007700"));
        }
        else{
            bookStatus.setText("Emprestado");
            bookStatus.setTextColor(Color.parseColor("#EE5500"));
        }

        if (Objects.equals(genre, "Artes")) {
            bookGenreIcon.setImageResource(R.drawable.ic_art_genre_24dp);
        }
        else if (Objects.equals(genre, "Biografias")) {
            bookGenreIcon.setImageResource(R.drawable.ic_biographies_genre_24dp);
        }
        else if (Objects.equals(genre, "Negócios")) {
            bookGenreIcon.setImageResource(R.drawable.ic_bussiness_genre_24dp);
        }
        else if (Objects.equals(genre, "Infantil")) {
            bookGenreIcon.setImageResource(R.drawable.ic_children_genre_24dp);
        }
        else if (Objects.equals(genre, "Culinária")) {
            bookGenreIcon.setImageResource(R.drawable.ic_cooking_genre_24dp);
        }
        else if (Objects.equals(genre, "Educação")) {
            bookGenreIcon.setImageResource(R.drawable.ic_education_genre_24dp);
        }
        else if (Objects.equals(genre, "Ficção e Literatura")) {
            bookGenreIcon.setImageResource(R.drawable.ic_fiction_litereture_genre_24dp);
        }
        else if (Objects.equals(genre, "História")) {
            bookGenreIcon.setImageResource(R.drawable.ic_history_genre_24dp);
        }
        else if (Objects.equals(genre, "Humor")) {
            bookGenreIcon.setImageResource(R.drawable.ic_humor_genre_24dp);
        }
        else if (Objects.equals(genre, "Mistério e Terror")) {
            bookGenreIcon.setImageResource(R.drawable.ic_mystery_genre_24dp);
        }
        else if (Objects.equals(genre, "Poesia")) {
            bookGenreIcon.setImageResource(R.drawable.ic_poetry_genre_24dp);
        }
        else if (Objects.equals(genre, "Religião e Espiritualidade")) {
            bookGenreIcon.setImageResource(R.drawable.ic_religion_genre_24dp);
        }
        else if (Objects.equals(genre, "Romance")) {
            bookGenreIcon.setImageResource(R.drawable.ic_romance_genre_24dp);
        }
        else if (Objects.equals(genre, "Ficcção cientifica e Fantasia")) {
            bookGenreIcon.setImageResource(R.drawable.ic_scrience_fiction_genre_24dp);
        }
        else if (Objects.equals(genre, "Autoajuda")) {
            bookGenreIcon.setImageResource(R.drawable.ic_self_help_genre_24dp);
        }

        loadOwner();

        preferences = new Preferences(BookDetailsActivity.this);
        String myListIds = preferences.getMyList();
        myList = new ArrayList<>(Arrays.asList(myListIds.split(" ")));

        if (Objects.equals(myList.get(0), "")){
            myList.clear();
        }

        if (myList.contains(id)){
            btAddMyList.setText("Remover da Lista");
        }
        else {
            btAddMyList.setText("Adicionar a lista");
        }

        btAddMyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myList.contains(id)){
                    removeFromMyList();
                }
                else {
                    addToMyList();
                }
            }
        });

        btLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(BookDetailsActivity.this, MapsActivity.class);
                intent1.putExtra("location", owner.getAddress() + " - " + owner.getNeighborhood() + " - " + owner.getCity());
                intent1.putExtra("bookName", name);
                intent1.putExtra("ownerName", owner.getName());
                startActivity(intent1);
            }
        });

    }

    private boolean removeFromMyList() {
        try {
            myList.remove(id);

            firebase = FirebaseConfig.getFirebase().child("user");
            firebase.child(preferences.getID()).child("myList").setValue(myList);
            Fragment4.myListAdapter.notifyDataSetChanged();
            Toast.makeText(BookDetailsActivity.this, "O livro foi removido da sua lista.", Toast.LENGTH_LONG).show();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean addToMyList() {
        try{
            myList.add(id);
            firebase = FirebaseConfig.getFirebase().child("user");
            firebase.child(preferences.getID()).child("myList").setValue(myList);
            Fragment4.myListAdapter.notifyDataSetChanged();
            Toast.makeText(BookDetailsActivity.this, "O livro foi adicionado a sua lista.", Toast.LENGTH_LONG).show();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private void loadOwner() {

        firebase = FirebaseConfig.getFirebase().child("user").child(ownerID);
        firebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                owner = dataSnapshot.getValue(User.class);
                ownerName.setText(owner.getName());
                ownerAddress.setText(owner.getAddress() + ", " + owner.getNeighborhood() + " - " + owner.getCity());
                layoutOwner.removeView(loadingOwner);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //Ação do botão de voltar
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finishAffinity();
                break;
            default:break;
        }
        return true;
    }
}
