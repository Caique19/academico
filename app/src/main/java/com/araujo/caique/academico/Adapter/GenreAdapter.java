package com.araujo.caique.academico.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.araujo.caique.academico.Entity.Genre;
import com.araujo.caique.academico.R;

import java.util.ArrayList;

/**
 * Created by Caique on 14/11/2017.
 */

public class GenreAdapter extends ArrayAdapter<Genre> {

    private final Context context;
    private final ArrayList<Genre> genres;

    public GenreAdapter(Context context, ArrayList<Genre> genres){
        super(context, R.layout.genre_item, genres);
        this.context = context;
        this.genres = genres;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.genre_item, parent, false);

        TextView genreName = (TextView) rowView.findViewById(R.id.genreName);
        ImageView genreIcon = (ImageView) rowView.findViewById(R.id.genreIcon);

        genreName.setText(genres.get(position).getName());
        genreIcon.setImageResource(genres.get(position).getIcon());

        return rowView;
    }
}
