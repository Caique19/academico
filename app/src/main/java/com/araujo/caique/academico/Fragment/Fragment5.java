package com.araujo.caique.academico.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.araujo.caique.academico.Activity.EditProfileActivity;
import com.araujo.caique.academico.Activity.LoginActivity;
import com.araujo.caique.academico.Activity.MainActivity;
import com.araujo.caique.academico.Activity.ShowBooksActivity;
import com.araujo.caique.academico.Adapter.BooksAdapter;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Caique on 13/11/2017.
 */

public class Fragment5 extends Fragment {

    private Toolbar mToolbar;
    private TextView userName;
    private TextView userCity;

    public static LinearLayout layoutMyBooks;
    public static LinearLayout layoutMyBooksTitle;
    public static ProgressBar loadingMyBooks;
    public static TextView textWithoutBooks;
    public static TextView btViewMore;
    public static TextView qtdMyBooks;

    private FirebaseAuth auth;
    private RecyclerView myBooksRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    public static RecyclerView.Adapter myBooksAdapter;

    private Preferences preferences;
    private ArrayList<String> myBooksIds;
    private ArrayList<Book> myBooks;

    private MenuItem btLogout;

    public Fragment5() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment5, container, false);

        preferences = MainActivity.preferences;
        myBooks = MainActivity.myBooks;
        myBooksIds = MainActivity.myBooksIds;

        auth = FirebaseAuth.getInstance();

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar_profile);
        userName = (TextView) view.findViewById(R.id.userNameProfile);
        userCity = (TextView) view.findViewById(R.id.userCityProfile);
        layoutMyBooks = (LinearLayout) view.findViewById(R.id.layoutMybooks);
        layoutMyBooksTitle = (LinearLayout) view.findViewById(R.id.layoutMybooksTitle);
        textWithoutBooks = (TextView) view.findViewById(R.id.textWithoutBooks);
        btViewMore = (TextView) view.findViewById(R.id.btViewMore);
        loadingMyBooks = (ProgressBar) view.findViewById(R.id.loadingMyBooks);
        qtdMyBooks = (TextView) view.findViewById(R.id.qtdMyBooks);
        myBooksRecyclerView = (RecyclerView) view.findViewById(R.id.myBooksRecyclerView);

        userName.setText(preferences.getName());
        userCity.setText(preferences.getCity());
        qtdMyBooks.setText("" + myBooksIds.size());

        myBooksRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        myBooksAdapter = new BooksAdapter(getContext(), myBooks);
        myBooksRecyclerView.setLayoutManager(layoutManager);
        myBooksRecyclerView.setAdapter(myBooksAdapter);

        MainActivity.loadMyBooks();

        btViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ShowBooksActivity.class);
                intent.putExtra("KEY", "MY_BOOKS");
                startActivity(intent);
            }
        });

        // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.action_exit){
                    auth.signOut();
                    getActivity().finish();
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                }
                else if (id == R.id.action_edit_profile){
                    Intent intent = new Intent(getContext(), EditProfileActivity.class);
                    startActivity(intent);
                }

                return true;
            }

        });

        // Inflate a menu to be displayed in the toolbar
        mToolbar.inflateMenu(R.menu.menu_profile);

        return view;
    }

}
