package com.araujo.caique.academico.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.araujo.caique.academico.Activity.ShowBooksActivity;
import com.araujo.caique.academico.Adapter.GenreAdapter;
import com.araujo.caique.academico.Entity.Genre;
import com.araujo.caique.academico.R;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Caique on 08/11/2017.
 */

public class Fragment2 extends Fragment {

    private String[] genresNames;

    private int[] genreIcons = new int[]{
            R.drawable.ic_art_genre_24dp,
            R.drawable.ic_biographies_genre_24dp,
            R.drawable.ic_bussiness_genre_24dp,
            R.drawable.ic_children_genre_24dp,
            R.drawable.ic_cooking_genre_24dp,
            R.drawable.ic_education_genre_24dp,
            R.drawable.ic_fiction_litereture_genre_24dp,
            R.drawable.ic_history_genre_24dp,
            R.drawable.ic_humor_genre_24dp,
            R.drawable.ic_mystery_genre_24dp,
            R.drawable.ic_poetry_genre_24dp,
            R.drawable.ic_religion_genre_24dp,
            R.drawable.ic_romance_genre_24dp,
            R.drawable.ic_scrience_fiction_genre_24dp,
            R.drawable.ic_self_help_genre_24dp};

    public Fragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment2, container, false);

        genresNames = getResources().getStringArray(R.array.genres);
        ListView listViewGenres = (ListView) view.findViewById(R.id.listViewGenres);
        ArrayAdapter adapter = new GenreAdapter(getContext(), createGenres());
        listViewGenres.setAdapter(adapter);


        listViewGenres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), ShowBooksActivity.class);
                intent.putExtra("KEY", genresNames[i]);
                startActivity(intent);
            }
        });

        return view;

    }

    private ArrayList<Genre> createGenres(){
        ArrayList<Genre> genres = new ArrayList<Genre>();
        for (int i=0; i < genresNames.length; i++){
            Genre g = new Genre(genresNames[i], genreIcons[i]);
            genres.add(g);
        }
        return genres;
    }
}
