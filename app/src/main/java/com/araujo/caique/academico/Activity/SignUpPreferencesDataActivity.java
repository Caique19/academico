package com.araujo.caique.academico.Activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.R;

import java.util.ArrayList;
import java.util.List;

public class SignUpPreferencesDataActivity extends AppCompatActivity {

    private ArrayList<String> favoriteGenresList = new ArrayList<String>();

    private Button btNext;
    private Button btPrevious;
    private TextView btSignIn;

    private CheckBox cbxBiography;
    private CheckBox cbxHistory;
    private CheckBox cbxBusiness;
    private CheckBox cbxPoetry;
    private CheckBox cbxFiction;
    private CheckBox cbxHumor;
    private CheckBox cbxArt;
    private CheckBox cbxCooking;
    private CheckBox cbxEducation;
    private CheckBox cbxRomance;
    private CheckBox cbxMystery;
    private CheckBox cbxFantasy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_preferences_data);

        Intent intent = getIntent();

        final String name = intent.getStringExtra("name");
        final String email = intent.getStringExtra("email");
        final String password = intent.getStringExtra("password");
        final String address = intent.getStringExtra("address");
        final String neighborhood = intent.getStringExtra("neighborhood");
        final String city = intent.getStringExtra("city");
        final String phone = intent.getStringExtra("phone");
        final String facebook = intent.getStringExtra("facebook");

        cbxBiography = (CheckBox) findViewById(R.id.cbxBiography);
        cbxHistory = (CheckBox) findViewById(R.id.cbxHistory);
        cbxBusiness = (CheckBox) findViewById(R.id.cbxBusiness);
        cbxPoetry = (CheckBox) findViewById(R.id.cbxPoetry);
        cbxFiction = (CheckBox) findViewById(R.id.cbxFiction);
        cbxHumor = (CheckBox) findViewById(R.id.cbxHumor);
        cbxArt = (CheckBox) findViewById(R.id.cbxArt);
        cbxCooking = (CheckBox) findViewById(R.id.cbxCooking);
        cbxEducation = (CheckBox) findViewById(R.id.cbxEducation);
        cbxRomance = (CheckBox) findViewById(R.id.cbxRomance);
        cbxMystery = (CheckBox) findViewById(R.id.cbxMystery);
        cbxFantasy = (CheckBox) findViewById(R.id.cbxFantasy);

        btNext = (Button) findViewById(R.id.btNext3);
        btNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (
                        !cbxBiography.isChecked() &&
                        !cbxHistory.isChecked() &&
                        !cbxBusiness.isChecked() &&
                        !cbxPoetry.isChecked() &&
                        !cbxFiction.isChecked() &&
                        !cbxHumor.isChecked() &&
                        !cbxArt.isChecked() &&
                        !cbxCooking.isChecked() &&
                        !cbxEducation.isChecked() &&
                        !cbxRomance.isChecked() &&
                        !cbxMystery.isChecked() &&
                        !cbxFantasy.isChecked()
                        ){
                    Toast.makeText(SignUpPreferencesDataActivity.this, "Selecione pelo menos um gênero de interesse", Toast.LENGTH_SHORT).show();
                }
                else{

                    if (cbxBiography.isChecked()){
                        favoriteGenresList.add(cbxBiography.getText().toString());
                    }
                    if (cbxHistory.isChecked()){
                        favoriteGenresList.add(cbxHistory.getText().toString());
                    }
                    if (cbxBusiness.isChecked()){
                        favoriteGenresList.add(cbxBusiness.getText().toString());
                    }
                    if (cbxPoetry.isChecked()){
                        favoriteGenresList.add(cbxPoetry.getText().toString());
                    }
                    if (cbxFiction.isChecked()){
                        favoriteGenresList.add(cbxFiction.getText().toString());
                    }
                    if (cbxHumor.isChecked()){
                        favoriteGenresList.add(cbxHumor.getText().toString());
                    }
                    if (cbxArt.isChecked()){
                        favoriteGenresList.add(cbxArt.getText().toString());
                    }
                    if (cbxCooking.isChecked()){
                        favoriteGenresList.add(cbxCooking.getText().toString());
                    }
                    if (cbxEducation.isChecked()){
                        favoriteGenresList.add(cbxEducation.getText().toString());
                    }
                    if (cbxRomance.isChecked()){
                        favoriteGenresList.add(cbxRomance.getText().toString());
                    }
                    if (cbxMystery.isChecked()){
                        favoriteGenresList.add(cbxMystery.getText().toString());
                    }
                    if (cbxFantasy.isChecked()){
                        favoriteGenresList.add(cbxFantasy.getText().toString());
                    }

                    Intent intent = new Intent(SignUpPreferencesDataActivity.this, SignUpTermsOfUseActivity.class);

                    intent.putExtra("name", name);
                    intent.putExtra("email", email);
                    intent.putExtra("password", password);
                    intent.putExtra("address", address);
                    intent.putExtra("neighborhood", neighborhood);
                    intent.putExtra("city", city);
                    intent.putExtra("phone", phone);
                    intent.putExtra("facebook", facebook);
                    intent.putStringArrayListExtra("favoriteGenres", favoriteGenresList);

                    startActivity(intent);
                }
            }
        });

        btPrevious = (Button) findViewById(R.id.btPrevious2);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btSignIn = (TextView) findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpPreferencesDataActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
