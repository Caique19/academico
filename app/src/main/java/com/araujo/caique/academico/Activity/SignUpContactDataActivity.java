package com.araujo.caique.academico.Activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.R;

import java.util.Objects;

public class SignUpContactDataActivity extends AppCompatActivity {

    private EditText address;
    private EditText neighborhood;
    private EditText city;
    private EditText phone;
    private EditText facebook;
    private Button btNext;
    private Button btPrevious;
    private TextView btSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up_contact_data);
        btNext = (Button) findViewById(R.id.btNext2);

        address = (EditText) findViewById(R.id.addressInput);
        neighborhood = (EditText) findViewById(R.id.neighborhoodInput);
        city = (EditText) findViewById(R.id.cityInput);
        phone = (EditText) findViewById(R.id.phoneInput);
        facebook = (EditText) findViewById(R.id.facebookInput);

        Intent intent = getIntent();

        final String name = intent.getStringExtra("name");
        final String email = intent.getStringExtra("email");
        final String password = intent.getStringExtra("password");

        btNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                if (Objects.equals(address.getText().toString(), "")){
                    Toast.makeText(SignUpContactDataActivity.this, "Informe o seu endereço", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(neighborhood.getText().toString(), "")){
                    Toast.makeText(SignUpContactDataActivity.this, "Informe o seu bairro", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(city.getText().toString(), "")){
                    Toast.makeText(SignUpContactDataActivity.this, "Informe a sua cidad e", Toast.LENGTH_SHORT).show();
                }
                else if (Objects.equals(phone.getText().toString(), "")){
                    Toast.makeText(SignUpContactDataActivity.this, "Informe um número para contato", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(SignUpContactDataActivity.this, SignUpPreferencesDataActivity.class);

                    intent.putExtra("name", name);
                    intent.putExtra("email", email);
                    intent.putExtra("password", password);
                    intent.putExtra("address", address.getText().toString());
                    intent.putExtra("neighborhood", neighborhood.getText().toString());
                    intent.putExtra("city", city.getText().toString());
                    intent.putExtra("phone", phone.getText().toString());
                    intent.putExtra("facebook", facebook.getText().toString());

                    startActivity(intent);
                }
            }
        });

        btPrevious = (Button) findViewById(R.id.btPrevious);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btSignIn = (TextView) findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpContactDataActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
