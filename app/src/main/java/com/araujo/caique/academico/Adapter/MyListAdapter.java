package com.araujo.caique.academico.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.R;

import java.util.ArrayList;


/**
 * Created by Caique on 11/12/2017.
 */

public class MyListAdapter extends ArrayAdapter<Book> {

    private final Context context;
    private final ArrayList<Book> booksInMylist;

    public MyListAdapter (Context context, ArrayList<Book> booksInMylist){
        super(context, R.layout.my_list_item, booksInMylist);
        this.context = context;
        this.booksInMylist = booksInMylist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.my_list_item, parent, false);

        TextView bookName = rowView.findViewById(R.id.nameMyList);
        TextView bookAuthor = rowView.findViewById(R.id.authorMyList);
        TextView bookPublishingCompany = rowView.findViewById(R.id.publishingCompanyMyList);
        TextView bookStatus = rowView.findViewById(R.id.statusMyList);

        bookName.setText(booksInMylist.get(position).getName());
        bookAuthor.setText(booksInMylist.get(position).getAuthor());
        bookPublishingCompany.setText(booksInMylist.get(position).getPublishingCompany());
        if (booksInMylist.get(position).isAvaliable()){
            bookStatus.setText("Disponível");
            bookStatus.setTextColor(Color.parseColor("#007700"));
        }
        else {
            bookStatus.setText("Emprestado");
            bookStatus.setTextColor(Color.parseColor("#EE5500"));
        }

        return rowView;

    }

}
