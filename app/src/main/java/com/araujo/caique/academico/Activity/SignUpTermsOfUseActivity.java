package com.araujo.caique.academico.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.araujo.caique.academico.Data.FirebaseConfig;
import com.araujo.caique.academico.Entity.User;
import com.araujo.caique.academico.Helper.Base64Custom;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class SignUpTermsOfUseActivity extends AppCompatActivity {

    private CheckBox cbxTermsOfUse;
    private Button btRegister;
    private Button btPrevious;
    private TextView btSignIn;
    private User user;
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_terms_of_use);

        Intent intent = getIntent();

        final String name = intent.getStringExtra("name");
        final String email = intent.getStringExtra("email");
        final String password = intent.getStringExtra("password");
        final String address = intent.getStringExtra("address");
        final String neighborhood = intent.getStringExtra("neighborhood");
        final String city = intent.getStringExtra("city");
        final String phone = intent.getStringExtra("phone");
        final String facebook = intent.getStringExtra("facebook");
        final ArrayList<String> favoriteGenres = intent.getStringArrayListExtra("favoriteGenres");

        cbxTermsOfUse = (CheckBox) findViewById(R.id.cbxTermsOfUse);

        progressDialog = new ProgressDialog(SignUpTermsOfUseActivity.this);
        progressDialog.setMessage("Criando conta...");

        btRegister = (Button) findViewById(R.id.btSignUpConfirm);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbxTermsOfUse.isChecked()){

                    user = new User();
                    user.setName(name);
                    user.setEmail(email);
                    user.setPassword(password);
                    user.setAddress(address);
                    user.setNeighborhood(neighborhood);
                    user.setCity(city);
                    user.setPhone(phone);
                    user.setFacebook(facebook);
                    user.setFavoriteGenres(favoriteGenres);

                    /*ArrayList<String> myBooks = new ArrayList<String>();
                    user.setMyBooks(myBooks);
                    ArrayList<String> myList = new ArrayList<String>();
                    user.setMyList(myList);
                    ArrayList<String> borrowedForMe = new ArrayList<String>();
                    user.setBorrowedForMe(borrowedForMe);*/

                    progressDialog.show();

                    registerUser();
                }
                else {
                    Toast.makeText(SignUpTermsOfUseActivity.this, "Você deve concordar com os termos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btPrevious = (Button) findViewById(R.id.btPrevious2);
        btPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btSignIn = (TextView) findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpTermsOfUseActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void registerUser(){

        auth = FirebaseConfig.getFirebaseAuth();
        auth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(SignUpTermsOfUseActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(SignUpTermsOfUseActivity.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();

                            FirebaseUser userFirebase = task.getResult().getUser();
                            user.setId(userFirebase.getUid());
                            user.register();

                            Preferences preferences = new Preferences(SignUpTermsOfUseActivity.this);
                            preferences.saveUserPreferences(userFirebase.getUid(), user.getName(), user.getEmail(), user.getCity(), user.getPassword(), null, null);

                            progressDialog.cancel();

                            Intent intent = new Intent(SignUpTermsOfUseActivity.this, LoginActivity.class);
                            intent.putExtra("name", user.getName());
                            startActivity(intent);
                            finish();
                        }
                        else {
                            String errorException = "";

                            try {
                                throw task.getException();
                            }
                            catch (FirebaseAuthWeakPasswordException e){
                                errorException = "Crie uma senha mais forte, contendo no mínimo 8 caracteres de letras e números.";
                            }
                            catch (FirebaseAuthInvalidCredentialsException e){
                                errorException = "O e-mail informado é invalido, informe um e-mail real.";
                            }
                            catch (FirebaseAuthUserCollisionException e){
                                errorException = "Este e-mail já está cadastrado!";
                            }
                            catch (Exception e){
                                errorException = "Erro ao efetuar o cadastro!";
                                e.printStackTrace();
                            }
                            Toast.makeText(SignUpTermsOfUseActivity.this, "Erro: " + errorException, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
