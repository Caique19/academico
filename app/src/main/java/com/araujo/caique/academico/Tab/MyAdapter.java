package com.araujo.caique.academico.Tab;

/**
 * Created by Caique on 08/11/2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.araujo.caique.academico.Fragment.Fragment1;
import com.araujo.caique.academico.Fragment.Fragment2;
import com.araujo.caique.academico.Fragment.Fragment3;
import com.araujo.caique.academico.Fragment.Fragment4;
import com.araujo.caique.academico.Fragment.Fragment5;
import com.araujo.caique.academico.R;

/**
 * Created by Ujang Wahyu on 18/08/2016.
 */

public class MyAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles ={"A","B","C","D", "E"};
    int[] icon = new int[]{R.drawable.ic_explore_black_24dp, R.drawable.ic_menu_white_24dp, R.drawable.ic_add_box_white_24dp, R.drawable.ic_my_requests_24dp, R.drawable.ic_person_white_24dp};
    private int heightIcon;

    public MyAdapter(FragmentManager fm, Context c){
        super(fm);
        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon=(int)(24*scale+0.5f);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag= null;

        if(position ==0){
            frag = new Fragment1();
        }else if(position == 1){
            frag = new Fragment2();
        }else if(position == 2){
            frag = new Fragment3();
        }else if(position == 3){
            frag = new Fragment4();
        }else if (position == 4){
            frag = new Fragment5();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    public CharSequence getPageTitle(int position){
        Drawable d = mContext.getResources().getDrawable(icon[position]);
        d.setBounds(0,0,heightIcon,heightIcon);
        ImageSpan is = new ImageSpan(d);

        SpannableString sp = new SpannableString(" ");
        sp.setSpan(is,0,sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sp;
    }

}