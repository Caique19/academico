package com.araujo.caique.academico.Data;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Caique on 21/11/2017.
 */

public class FirebaseConfig {

    private static DatabaseReference firebaseReference;
    private static FirebaseAuth auth;

    public static DatabaseReference getFirebase(){
        if (firebaseReference == null) {
            firebaseReference = FirebaseDatabase.getInstance().getReference();
        }
        return firebaseReference;
    }

    public  static FirebaseAuth getFirebaseAuth(){
        if (auth == null) {
            auth = FirebaseAuth.getInstance();
        }
        return auth;
    }

}
