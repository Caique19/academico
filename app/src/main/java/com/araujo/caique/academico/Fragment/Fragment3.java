package com.araujo.caique.academico.Fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.araujo.caique.academico.Data.FirebaseConfig;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.Helper.Base64Custom;
import com.araujo.caique.academico.Helper.Preferences;
import com.araujo.caique.academico.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Caique on 08/11/2017.
 */

public class Fragment3 extends Fragment {

    private EditText bookNameInput;
    private EditText bookAuthorInput;
    private Spinner genresSpinner;
    private EditText bookPublishingCompanyInput;
    private EditText bookSynopsisInput;
    private Button btAddBook;
    private Book book;
    private Preferences userPreferences;
    private DatabaseReference firebase;
    private ArrayList<String> myBooks = new ArrayList<String>();

    public Fragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment3, container, false);

        userPreferences = new Preferences(getContext());

        firebase = FirebaseConfig.getFirebase().child("user").child(userPreferences.getID()).child("myBooks");

        if (firebase != null){
            firebase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    myBooks.clear();

                    for (DataSnapshot data : dataSnapshot.getChildren()){
                        String myBooksCurrent = data.getValue().toString();

                        myBooks.add(myBooksCurrent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        bookNameInput = (EditText) view.findViewById(R.id.bookNameInput);
        bookAuthorInput = (EditText) view.findViewById(R.id.bookAuthorInput);
        bookPublishingCompanyInput = (EditText) view.findViewById(R.id.bookPublishingCompanyInput);
        bookSynopsisInput = (EditText) view.findViewById(R.id.bookSynopsisInput);

        genresSpinner = (Spinner) view.findViewById(R.id.bookGenreSpinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.genres_spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genresSpinner.setAdapter(adapter);

        btAddBook = (Button) view.findViewById(R.id.btAddBook);
        btAddBook.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                if (Objects.equals(bookNameInput.getText().toString(), "")){
                    Toast.makeText(getContext(), "Informe o nome do livro", Toast.LENGTH_LONG).show();
                }
                else if (Objects.equals(bookAuthorInput.getText().toString(), "")){
                    Toast.makeText(getContext(), "Informe o autor do livro", Toast.LENGTH_LONG).show();
                }
                else if (Objects.equals(genresSpinner.getSelectedItem().toString(), "Selecione o gênero")){
                    Toast.makeText(getContext(), "Informe o gênero do livro", Toast.LENGTH_LONG).show();
                }
                else if (Objects.equals(bookPublishingCompanyInput.getText().toString(), "")){
                    Toast.makeText(getContext(), "Informe a editora do livro", Toast.LENGTH_LONG).show();
                }
                else if (Objects.equals(bookSynopsisInput.getText().toString(), "")){
                    Toast.makeText(getContext(), "Informe a sinopse do livro", Toast.LENGTH_LONG).show();
                }
                else {

                    book = new Book();
                    book.setName(bookNameInput.getText().toString());
                    book.setAuthor(bookAuthorInput.getText().toString());
                    book.setPublishingCompany(bookPublishingCompanyInput.getText().toString());
                    book.setGenre(genresSpinner.getSelectedItem().toString());
                    book.setSynopsis(bookSynopsisInput.getText().toString());
                    book.setAvaliable(true);
                    book.setOwnerID(userPreferences.getID());

                    String idBook = Base64Custom.base64Encoding(book.getName() + userPreferences.getID());
                    book.setId(idBook);

                    registerBook(book);
                }
            }
        });

        return view;
    }

    private boolean registerBook(Book book) {
        try{

            firebase = FirebaseConfig.getFirebase().child("book");
            firebase.child(book.getId()).setValue(book);
            myBooks.add(book.getId());

            firebase = FirebaseConfig.getFirebase().child("user");
            firebase.child(userPreferences.getID()).child("myBooks").setValue(myBooks);

            Toast.makeText(getContext(), "Livro cadastrado com sucesso!", Toast.LENGTH_LONG).show();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
