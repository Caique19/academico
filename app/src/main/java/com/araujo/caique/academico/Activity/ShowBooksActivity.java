package com.araujo.caique.academico.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import com.araujo.caique.academico.Adapter.BooksAdapter;
import com.araujo.caique.academico.Adapter.showBooksAdapter;
import com.araujo.caique.academico.Entity.Book;
import com.araujo.caique.academico.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;


/**
 * Created by ana_k on 12/12/2017.
 */

public class ShowBooksActivity extends AppCompatActivity {

    private LinearLayout layout;
    private GridView gridView;
    private ListAdapter adapter;

    private String KEY;

    private ArrayList<Book> books;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_books);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Mostrar botão de voltar
        getSupportActionBar().setHomeButtonEnabled(true); // Ativar botão de voltar
        setTitle("Adicionados Recentemente");

        Intent intent = getIntent();
        KEY = intent.getStringExtra("KEY");

        if (Objects.equals(KEY, "RECENT_BOOKS")) {
            books = MainActivity.allBooks;
        }
        else if (Objects.equals(KEY, "MY_BOOKS")) {
            books = MainActivity.myBooksFull;
        }
        else if (Objects.equals(KEY, "Artes")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Artes")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Biografias")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Biografias")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Negócios")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Negócios")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Infantil")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Infantil")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Culinária")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Culinária")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Educação")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Educação")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Ficção e Literatura")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Ficção e Literatura")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "História")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "História")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Humor")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Humor")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Mistério e Terror")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Mistério e Terror")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Poesia")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Poesia")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Religião e Espiritualidade")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Religião e Espiritualidade")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Romance")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Romance")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Ficção cientifica e Fantasia")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Ficção cientifica e Fantasia")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }
        else if (Objects.equals(KEY, "Autoajuda")) {
            books = new ArrayList<>();
            for (int i = 0; i < MainActivity.allBooks.size(); i++) {
                if (Objects.equals(MainActivity.allBooks.get(i).getGenre(), "Autoajuda")){
                    books.add(MainActivity.allBooks.get(i));
                }
            }
        }

        Log.v("LIVROS", "" + books);

        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new showBooksAdapter(this, books);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(ShowBooksActivity.this, BookDetailsActivity.class);
                intent.putExtra("id", books.get(position).getId());
                intent.putExtra("name", books.get(position).getName());
                intent.putExtra("author", books.get(position).getAuthor());
                intent.putExtra("publishingCompany", books.get(position).getPublishingCompany());
                intent.putExtra("status", books.get(position).isAvaliable());
                intent.putExtra("synopsis", books.get(position).getSynopsis());
                intent.putExtra("genre", books.get(position).getGenre());
                intent.putExtra("owner", books.get(position).getOwnerID());
                startActivity(intent);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:break;
        }
        return true;
    }
}